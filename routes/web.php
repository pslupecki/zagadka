<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $app->get('/', function () use ($app) {
//     return $app->version();
// });



$app->group(['prefix' => '/'], function($app)
{
    $app->get('content','DashboardController@index');

    $app->get('content/{id}','DashboardController@show');

    $app->post('content','DashboardController@create');

    $app->put('content/{id}','DashboardController@update');

    $app->delete('content/{id}','DashboardController@delete');
});
