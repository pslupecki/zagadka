$( document ).ready(function() {

  $(function(){
      $("td").click(function(){

        $(this).find('span').addClass("hidden");
        $(this).find('input').removeClass('hidden').addClass('visible');
        $(this).find('input').focus();
          });

        $( "input" ).blur(function() {
          $(this).prev().addClass("visible").removeClass("hidden");
          $(this).removeClass('visible').addClass('hidden');
          var obj=$(this);
          var value=$(this).val();

          var text_id= $(this).attr('data-id');
          $.ajax({
        		 url: "/content/"+text_id,
        		 type: "PUT",
        		 data: {
        			 content:value,
               id:text_id
        		 },
        		 success: function( result ) {
               if(result.status==1){
                 obj.prev().text(value);
                 obj.prev().addClass("visible").removeClass("hidden");
                  }
        			}
			      });

        });

    });

});
