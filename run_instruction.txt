/**
 * Guide for run this app This app required PHP 5.6 or higher and mysql db
 * 1. First step get te pull from git server
 * 2. Make database  with name homestead
 * 3. Run this project on localhost or remote server
 * 4.File .env has short configuration for db
 * 5. Configure Your database and set CHMOD -R 777 to storage and vendor folder
 * 6. If server have ssh you can put command php artisan migrate this build db automatically
 * 7. If not read the sql file to phpymyadmin or direclty to mysql
 */
