<?php

use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('content')->insert([
        'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et consequat enim, non tristique dolor. Nulla consequat facilisis dolor vel pretium. Aenean vitae ultricies ante. Nunc ac enim ante',

    ]);

      DB::table('content')->insert([
          'value' => 'Cras lacinia tellus sit amet nisl euismod placerat. Nullam tristique, eros ut tempor convallis, orci ipsum bibendum nisi, eget pulvinar orci sem eu nulla.',

      ]);

      DB::table('content')->insert([
          'value' => 'Integer in dui sodales, rutrum velit et, placerat tortor. Aenean nec sapien nibh. Praesent porttitor egestas eros, in sodales est lacinia nec.	Delete',

      ]);


    }
}
