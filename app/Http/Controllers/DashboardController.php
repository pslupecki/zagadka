<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Content;
use DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * [index description]
     * @method index
     * @return [type] [description]
     */

    public function index()
    {

      $contents=app('db')->select("SELECT * FROM content");

      return view('frontend.index', ['content' =>$contents]);
    }

    /**
     * [show description]
     * @method show
     * @return [type] [description]
     */

    public function show($id)
    {

      $contents=app('db')->select("SELECT * FROM content WHERE `id`='.$id.'");

      return view('frontend.index', ['content' =>$contents]);
    }


     /**
      * [update description]
      * @method update
      * @return [type] [description]
      */

     public function update(Request $request)
     {
       $value=$request->input('content');
       $id=$request->input('id');
       $contents=app('db')->update("UPDATE `content` SET value ='$value' WHERE `id`='$id'");

       return response()->json(['status' => $contents]);

     }

     /**
      * [delete description]
      * @method delete
      * @return [type] [description]
      */

    public function delete()
    {

      $contents=app('db')->select("DELETE  FROM content WHERE `id`='.$id.'");

      return redirect('/content');
    }






    //
}
