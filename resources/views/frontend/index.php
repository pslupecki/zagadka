<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../../favicon.ico">

  <title>Notebook</title>

  <!-- Bootstrap core CSS -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/base.js"></script>
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
              aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Notebook</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
<div class="container">
  <div class="starter-template">
    <table class='table table-bordered'>
      <thead>
      <tr>
        <td>Id</td>
        <td>Note</td>
        <td style="width:200px">Actions</td>
      </tr>
      </thead>
      <tbody>
        <?php foreach ($content as $key=>$field): ?>
          <tr>
            <td><?=$field->id;?></td>
            <td><span><?=$field->value;?></span>
              <input type="text" data-id="<?=$field->id;?>" value="<?=$field->value;?>" class="hidden"/>
            </td>
            <td>
              <button class="btn btn-danger">Delete</button>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div><!-- /.container -->


<style>
  body {
    padding-top : 50px;
  }
  input {
    min-height:50px;
  }
  .hidden{
    display:none;
  }
  .visible{
    display:inline;
    width:100%;
  }

  .starter-template {
    padding    : 40px 15px;
    text-align : center;
  }
</style>


</body>
</html>
